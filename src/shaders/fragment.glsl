#version 330 core

in Attribs {
	vec3 N;
	vec3 L;
	vec3 O;
	vec2 texCoord;
} AttribsIn;

out vec4 FragColor;

uniform sampler2D tex;

void main()
{
	vec3 N_u = normalize( AttribsIn.N );
	vec3 L_u = normalize( AttribsIn.L );
	vec3 O_u = normalize( AttribsIn.O );

	float NdotL = max( abs(dot(N_u, L_u)), 0.0 );
	if(dot(N_u, L_u) < 0)
		NdotL = 0;
	float NdotHV = max( 0.0, dot( reflect( -L_u, N_u ), O_u ) ); // Phong

	vec4 texel = texture(tex, AttribsIn.texCoord);
	
	vec4 ambient = texel * 0.1;
	vec4 diffuse = texel * 0.6 * NdotL;
	vec4 specular = texel * 0.9 * pow( NdotHV, 100 );
	
	FragColor = ambient + diffuse + specular;
}