#version 330 core

in Attribs {
	vec3 texCoord;
} AttribsIn;

out vec4 FragColor;

uniform samplerCube tex;

void main()
{
	FragColor = texture(tex, AttribsIn.texCoord);
}