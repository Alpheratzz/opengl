#version 330 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in Attribs {
	vec3 N;
	vec3 L;
	vec3 O;
	vec2 texCoord;
} AttribsIn[];

out Attribs {
	vec3 N;
	vec3 L;
	vec3 O;
	vec2 texCoord;
} AttribsOut;

void main()
{
	for ( int i = 0 ; i < gl_in.length() ; ++i )
	{
		gl_Position = gl_in[i].gl_Position;
		AttribsOut.N = AttribsIn[i].N;
		AttribsOut.L = AttribsIn[i].L;
		AttribsOut.O = AttribsIn[i].O;
		AttribsOut.texCoord = AttribsIn[i].texCoord;

		EmitVertex();
	}
}
