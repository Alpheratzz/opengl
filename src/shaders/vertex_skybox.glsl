#version 330 core

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;

// Input vertex data, different for all executions of this shader.
in vec4 Vertex;

out Attribs {
	vec3 texCoord;
} AttribsOut;

void main()
{
	AttribsOut.texCoord = vec3(Vertex);
	gl_Position = matrProj * matrVisu * matrModel * Vertex;
}

