#ifndef SHADER_PIPELINE_H
#define SHADER_PIPELINE_H

#include <fstream>
#include <sstream>
#include <iostream>

#include "PipelineMatrix.h"

class ShaderPipeline
{
public:
	ShaderPipeline( ) : prog_(0) { }
	~ShaderPipeline( ) { glDeleteProgram( prog_ ); }
	GLuint id( ) { return prog_; }
	ShaderPipeline& operator=( GLuint prog ) { prog_ = prog; return *this; }

	void create( )
	{
		if ( prog_ ) glDeleteProgram( prog_ );
		prog_ = glCreateProgram();
	}

	void use( ) { glUseProgram( prog_ ); }

	void send( const GLchar *name, GLint v0 ) { glUniform1i( glGetUniformLocation( prog_, name ), v0 ); }
	void send( const GLchar* name, glm::mat3& val ) { glUniformMatrix3fv( glGetUniformLocation( prog_, name ), 1, GL_FALSE, glm::value_ptr(val) ); }
	void send( const GLchar* name, glm::mat4& val ) { glUniformMatrix4fv( glGetUniformLocation( prog_, name ), 1, GL_FALSE, glm::value_ptr(val) ); }
	void send( const GLchar* name, PipelineMatrix& val ) { glUniformMatrix4fv( glGetUniformLocation( prog_, name ), 1, GL_FALSE, glm::value_ptr(val.getMatr()) ); }

	GLuint getLocation( const GLchar* name )
	{
		GLint loc = glGetAttribLocation( prog_, name );
		if ( loc == -1 ) std::cerr << "!!! " << name << ": not found" << std::endl;
		return( loc );
	}
	void setLocation( GLuint index, const GLchar* name ) { glBindAttribLocation( prog_, index, name ); }

	const GLchar* readShader( const GLchar* fil )
	{
		std::ifstream file( fil );
		if ( file.fail() )
		{
			std::cerr << "!!! " << fil << ": " << strerror(errno) << std::endl;
			return NULL;
		}

		std::stringstream fileContent;
		fileContent << file.rdbuf();
		file.close();

		std::string content = fileContent.str();
		const int size = content.size();

		char *source = new char[size+1];
		strcpy( source, content.c_str() );
		return source;
	};

	bool attach( GLuint type, const GLchar* fil )
	{
		const GLchar* source = readShader( fil );
		if ( source == NULL ) return( false );

		GLuint shaderID = glCreateShader( type );
		glShaderSource( shaderID, 1, &source, NULL );
		glCompileShader( shaderID );
		glAttachShader( prog_, shaderID );
		delete [] source;

		int infologLength = 0;
		glGetShaderiv( shaderID, GL_INFO_LOG_LENGTH, &infologLength );
		if ( infologLength > 1 )
		{
			char* infoLog = new char[infologLength+1];
			int charsWritten = 0;
			glGetShaderInfoLog( shaderID, infologLength, &charsWritten, infoLog );
			std::cout  << fil << std::endl << infoLog << std::endl;
			delete[] infoLog;
			return( false );
		}
		return( true );
	};

	bool link( )
	{
		glLinkProgram( prog_ );

		int infologLength = 0;
		glGetProgramiv( prog_, GL_INFO_LOG_LENGTH, &infologLength );
		if ( infologLength > 1 )
		{
			char* infoLog = new char[infologLength+1];
			int charsWritten = 0;
			glGetProgramInfoLog( prog_, infologLength, &charsWritten, infoLog );
			std::cout << "prog_" << std::endl << infoLog << std::endl;
			delete[] infoLog;
			return( false );
		}
		return( true );
	}

private:
	GLuint prog_;
};

#endif // SHADER_PIPELINE_H
