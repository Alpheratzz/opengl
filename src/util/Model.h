#ifndef MODEL_H
#define MODEL_H

/* STD */
#include <string>

#include "ShaderPipeline.h"

class Model
{
public:
	Model(ShaderPipeline* program, std::string filename);
	~Model();

	void load();

	void draw();

private:
	ShaderPipeline* program;
	GLuint VertexArrayID;
	GLuint vbo[3];

	float* vertexArray;
	float* normalArray;
	float* uvArray;
	int numVerts;
};

#endif // MODEL_H
