/* Assimp */
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

/* GLEW */
#include <GL/glew.h>

#include "Model.h"

using namespace std;

Model::Model(ShaderPipeline* program, std::string filename) : program(program)
{
	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(filename, aiProcessPreset_TargetRealtime_Fast);
	aiMesh *mesh = scene->mMeshes[0];

	numVerts = mesh->mNumFaces*3; 
	vertexArray = new float[mesh->mNumFaces*3*3];
	normalArray = new float[mesh->mNumFaces*3*3];
	uvArray = new float[mesh->mNumFaces*3*2];
	
	for(unsigned int i=0;i<mesh->mNumFaces;i++)
	{
		const aiFace& face = mesh->mFaces[i];
		 
		for(int j=0;j<3;j++)
		{
			aiVector3D uv = mesh->mTextureCoords[0][face.mIndices[j]];
			memcpy(uvArray,&uv,sizeof(float)*2);
			uvArray+=2;
			 
			aiVector3D normal = mesh->mNormals[face.mIndices[j]];
			memcpy(normalArray,&normal,sizeof(float)*3);
			normalArray+=3;
			 
			aiVector3D pos = mesh->mVertices[face.mIndices[j]];
			memcpy(vertexArray,&pos,sizeof(float)*3);
			vertexArray+=3;
		}
	}
	
	uvArray-=mesh->mNumFaces*3*2;
	normalArray-=mesh->mNumFaces*3*3;
	vertexArray-=mesh->mNumFaces*3*3;
}

Model::~Model()
{
	delete vertexArray;
	delete normalArray;
	delete uvArray;

	glDeleteBuffers(3, vbo);
	glDeleteVertexArrays(1, &VertexArrayID);
}

void Model::load()
{
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
	glGenBuffers(3, vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * numVerts * 3, vertexArray, GL_STATIC_DRAW);
	GLint locVertex = program->getLocation("Vertex");
	glVertexAttribPointer(locVertex, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(locVertex);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * numVerts * 3, normalArray, GL_STATIC_DRAW);
	GLint locNormal = program->getLocation("Normal");
	glVertexAttribPointer(locNormal, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(locNormal);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * numVerts * 2, uvArray, GL_STATIC_DRAW);
	GLint locTexture = program->getLocation("Texture");
	glVertexAttribPointer(locTexture, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(locTexture);
}

void Model::draw()
{
	glBindVertexArray(VertexArrayID);
	glDrawArrays(GL_TRIANGLES, 0, numVerts);
}
