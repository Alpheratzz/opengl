#ifndef TEXTURE_H
#define TEXTURE_H

/* GLEW */
#include <GL/glew.h>

/* libpng */
#include <png.h>

class Texture
{
public:
	Texture(const char * file_name, int * width, int * height);
	~Texture();

	GLuint tex2D();

	png_byte* data();

private:
	png_byte* image_data;
	png_bytep* row_pointers;
	png_structp png_ptr;
	png_infop info_ptr;
	png_infop end_info;
	png_uint_32 temp_width, temp_height;
};

#endif // TEXTURE_H
