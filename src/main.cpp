/* STD headers */
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>

/* GLEW */
#include <GL/glew.h>

/* GLFW */
#include <glfw3.h>

#include "util/PipelineMatrix.h"
#include "util/Model.h"
#include "util/ShaderPipeline.h"
#include "util/Texture.h"


using namespace std;
using namespace glm;

ShaderPipeline prog;
ShaderPipeline skybox;

GLFWwindow* window;

PipelineMatrix matrModel;
PipelineMatrix matrVisu;
PipelineMatrix matrProj;

dvec3 cam(25.0, 0.0, 0.2);

dvec2 mouse_pos(400, 300);

bool pressed = false;
bool pressed_l = false;
bool pressed_r = false;

void clamp_camera()
{
	cam[0] = clamp(cam[0], 0.1, 100.0);
	cam[2] = clamp(cam[2], -(3.141592 / 2 - 0.1), 3.141592 / 2 - 0.1);
}

void key_callback( GLFWwindow* window, int key, int scancode, int action, int mods )
{
	
}

void mouse_button_callback( GLFWwindow* window, int button, int action, int mods )
{
	double xpos, ypos; glfwGetCursorPos( window, &xpos, &ypos );

	pressed = action == GLFW_PRESS;
	
	if (pressed)
	{
		mouse_pos.x = xpos;
		mouse_pos.y = ypos;

		if(button == GLFW_MOUSE_BUTTON_1)
			pressed_l = true;
		if(button == GLFW_MOUSE_BUTTON_2)
			pressed_r = true;
	}
	else
	{
		if(button == GLFW_MOUSE_BUTTON_1)
			pressed_l = false;
		if(button == GLFW_MOUSE_BUTTON_2)
			pressed_r = false;
	}
}

void cursor_position_callback( GLFWwindow* window, double xpos, double ypos )
{
	double deltaX = xpos - mouse_pos.x;
	double deltaY = ypos - mouse_pos.y;

	if(pressed_r)
	{
		cam[2] += deltaY * 0.01;
		cam[1] -= deltaX * 0.01;
	}

	clamp_camera();

	mouse_pos.x = xpos;
	mouse_pos.y = ypos;
}

void scroll_callback( GLFWwindow* window, double xoffset, double yoffset )
{
	cam[0] -= yoffset * 1.5;
	clamp_camera();
}

int main( void )
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	// Open a window and create its OpenGL context
	//GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	//const GLFWvidmode* mode = glfwGetVideoMode(monitor);

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_REFRESH_RATE, GLFW_DONT_CARE);

	//window = glfwCreateWindow(mode->width, mode->height, "OpenGL Experiments", monitor, NULL);
	window = glfwCreateWindow(1366, 768, "OpenGL Experiments", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	glfwSetKeyCallback( window, key_callback );
	glfwSetMouseButtonCallback( window, mouse_button_callback );
	glfwSetCursorPosCallback( window, cursor_position_callback );
	glfwSetScrollCallback( window, scroll_callback );

	// Dark grey background
	glClearColor(0.1f, 0.1f, 0.1f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); 
	glEnable(GL_CULL_FACE);

	
	prog.create();
	prog.attach(GL_VERTEX_SHADER, "shaders/vertex.glsl");
	prog.attach(GL_GEOMETRY_SHADER, "shaders/geometry.glsl" );
	prog.attach(GL_FRAGMENT_SHADER, "shaders/fragment.glsl");
	prog.link();

	skybox.create();
	skybox.attach(GL_VERTEX_SHADER, "shaders/vertex_skybox.glsl");
	skybox.attach(GL_FRAGMENT_SHADER, "shaders/fragment_skybox.glsl");
	skybox.link();

	vector<const GLchar*> faces;
	faces.push_back("textures/GalaxyTex_PositiveX.png");
	faces.push_back("textures/GalaxyTex_NegativeX.png");
	faces.push_back("textures/GalaxyTex_PositiveY.png");
	faces.push_back("textures/GalaxyTex_NegativeY.png");
	faces.push_back("textures/GalaxyTex_PositiveZ.png");
	faces.push_back("textures/GalaxyTex_NegativeZ.png");

	GLuint textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width,height;

	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
	for(GLuint i = 0; i < faces.size(); i++)
	{
		Texture* t = new Texture(faces[i], &width, &height);
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
			GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, t->data()
		);
		delete t;
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	Model skyboxCube = Model(&skybox, "models/skybox.obj");
	skyboxCube.load();


	Texture* t = new Texture("textures/texture.png", NULL, NULL);
	GLuint torusTex = t->tex2D();
	delete t;
	Model torus = Model(&prog, "models/cube.obj");
	torus.load();

	do{
		if (glfwGetKey( window, GLFW_KEY_UP ) == GLFW_PRESS)
			cam[2] += 0.03;
		if (glfwGetKey( window, GLFW_KEY_DOWN ) == GLFW_PRESS)
			cam[2] -= 0.03;
		if (glfwGetKey( window, GLFW_KEY_RIGHT ) == GLFW_PRESS)
			cam[1] += 0.03;
		if (glfwGetKey( window, GLFW_KEY_LEFT ) == GLFW_PRESS)
			cam[1] -= 0.03;
		clamp_camera();

		// Clear the screen
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );


		/* Compute all matrices */
		matrProj.Perspective( 45.0, (float)1366 / 768, 0.1, 300.0 );
		matrVisu.LookAt( cam[0]*cos(cam[2])*cos(cam[1]), cam[0]*cos(cam[2])*sin(cam[1]), cam[0]*sin(cam[2]),  0, 0, 0,  0, 0, 1 );
		mat4 skyboxView = mat4(mat3(matrVisu.getMatr())); // removing the translation
		matrModel.LoadIdentity();
		mat3 matrVM = mat3( matrVisu.getMatr()*matrModel.getMatr() );
		mat3 matrNormale = transpose( inverse( matrVM ) );


		// Draw the skybox
		skybox.use();
		glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
		
		skybox.send("matrProj", matrProj);
		skybox.send("matrVisu", skyboxView);
		skybox.send("matrModel", matrModel);

		glDepthMask(GL_FALSE);
			matrModel.PushMatrix();
				matrModel.Rotate(90, 1, 0, 0);
				skybox.send("matrModel", matrModel);
				skyboxCube.draw();
			matrModel.PopMatrix();
		glDepthMask(GL_TRUE);


		// Draw the torus
		prog.use();
		glBindTexture(GL_TEXTURE_2D, torusTex);
		prog.send("matrProj", matrProj);
		prog.send("matrVisu", matrVisu);
		prog.send("matrModel", matrModel);
		prog.send("matrNormale", matrNormale);
		torus.draw();
		
		glfwSwapBuffers(window);
		glfwPollEvents();
		

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}
